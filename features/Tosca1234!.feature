Feature: Guess the word
  <Some interesting description here>

  Scenario: Guess the word
    <Some interesting scenario steps here>
    When The Maker starts a game
    Then The Maker waits for a player to join
    
    Scenario: Make Money
      Given As a parent with online banking access
      When I log in to my account with my username and password
      Then I should be able to add a child to my account
      And My child should have access to my account
