Feature: As a parent I should see link to add new kid to my online bank account
  <As a parent should see link to add new kid>

  Scenario: As a parent I should see the link to add new kid to my account
    <As a parent should see link to add new kid>
    
    Given That I am a parent and have a child
    When I go my online bank website
    And I should be able to log into my account
    Then I should see my online account balance
    And I should see a link to add a new kid to all my online bank accounts
    And I should be able to log into my bank account
    And I should see my online bank account details
    
