Feature: Withdraw money
  <Some interesting description here>

  Scenario: Money machine
    Given I have a Debit card
    And I put it into the money machine
    And I enter the right pin
    Then I select any amount I want
    Then I am able to withdraw up to 400 Euro per day

  Scenario: kid should log in
    Given I am a kid with an account
    And my parents have transferred some money to my account
    When I log in to my account with my username and password
    Then I should see my total account balance
    